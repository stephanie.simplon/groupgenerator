const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();
var cors = require('cors');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(cors());

mongoose.connect('mongodb://localhost:27018/test', { useNewUrlParser: true }, function (err) {
    if (err) { throw err; }
});

var Schema = mongoose.Schema;
var FirstnameSchema = new Schema({
    firstname: String,
}, {
    versionKey: false
});

var Users = mongoose.model('Users', FirstnameSchema);

var SaveSchema = new Schema({
    gen: Array,
    comment: String,
    date: String
}, {
    versionKey: false
});

var SavedPolynomes = mongoose.model('SavedPolynome', SaveSchema);

//Ajoute un nouveau prénom
app.post("/new/user", (req, res) => {
    var user_instance = new Users({ firstname: req.body.firstname });
    user_instance.save(function (err) {
        if (err) return handleError(err);
    });
    res.send();
});

//Génère et renvoie les polynômes formés
app.get("/generate/:nbPolynome", async (req, res) => {
    try {
        var tab = [];
        var tabFinal = [];
        var tabPolynome = [];
        var nbPolynome = req.params.nbPolynome; //version initiale : binôme (2)
        var tabTemp = await Users.find({}, { firstname: 1, _id: 0 });
        for (i = 0; i < tabTemp.length; i++) {
            tab.push(tabTemp[i].firstname);
        }
        while (tab.length > 0) {
            let name = tab.splice(Math.floor(Math.random() * tab.length), 1);
            tabPolynome.push(name[0]);
            if (tabPolynome.length == nbPolynome) {
                tabFinal.push(tabPolynome.splice(0, nbPolynome));
            } else if (tab.length === 0) {
                tabFinal.push(tabPolynome);
            }
        }
        res.send(tabFinal);
    } catch (error) {
        console.error(error);
    }
});

//Renvoie la liste des prénoms
app.get("/userlist", async (req, res) => {
    try {
        var tab = [];
        var tabTemp = await Users.find({}, { firstname: 1, _id: 0 }).sort({ firstname: 1 });
        for (i = 0; i < tabTemp.length; i++) {
            tab.push(tabTemp[i].firstname);
        }
        res.send(tab);
    } catch (error) {
        console.error(error);
    }
});

//Enregistre un polynôme généré
app.post("/save", (req, res) => {
    var save_instance = new SavedPolynomes({ date: req.body.date, comment: req.body.comment, gen: req.body.gen });
    save_instance.save(function (err) {
        if (err) return handleError(err);
    });
    res.send();
});

//Renvoie l'historique des polynômes enregistrés
app.get("/saved", async (req, res) => {
    try {
        var Polynomes = await SavedPolynomes.find({}, { date: 1, comment: 1, gen: 1, _id: 0}).sort({ date: -1});
        res.send(Polynomes);
    } catch (error) {
        console.error (error);
    }
});

app.listen(3000);