import { Component, OnInit, Input, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-names',
  templateUrl: './names.component.html',
  styleUrls: ['./names.component.scss']
})
export class NamesComponent implements OnInit, OnChanges {

  @Input("names") namesTab: Array<string>;
  
  private tab: Array<string>;

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.dataService.get_firstnames().subscribe((res: any[]) => {
      this.tab = res;
    });
  }  

  ngOnChanges(changes: SimpleChanges) {
      this.tab = changes.namesTab.currentValue;
  }

}
