import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  baseUrl: string = "http://localhost:3000";

  constructor(private httpClient: HttpClient) { }

  get_firstnames() {
    return this.httpClient.get(this.baseUrl + '/userlist');
  }
  get_polynomes(nb: number) {
    return this.httpClient.get(this.baseUrl + '/generate/' + nb);
  }
  get_saves() {
    return this.httpClient.get(this.baseUrl + '/saved');
  }
}