import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../data.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {

  addCommentValue : String;
  private groupsDate: Date;

  constructor(private http: HttpClient, private dataService: DataService) {
  }

  ngOnInit() {
  }

  @Input("groups") groupsTab: Array<string>;
  @Output() newSave = new EventEmitter<any>();

  onFormSubmit(userForm: NgForm) {
    if (userForm.value.comment && this.groupsTab.length != 0) {
      this.groupsDate = new Date();
      this.http.post('http://localhost:3000/save', {
        comment: userForm.value.comment,
        date: this.groupsDate.toLocaleDateString("fr-FR") + " " + this.groupsDate.toLocaleTimeString('fr-FR'),
        gen: this.groupsTab
      })
      .subscribe(
        res => {
          this.addCommentValue = '';
          this.dataService.get_saves().subscribe((res: any[]) => {
            this.newSave.emit(res);
          })
        },
        err => {
          console.log("Error occured");
        }
      );
    }
  }

}