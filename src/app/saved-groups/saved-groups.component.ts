import { Component, OnInit, SimpleChanges, Input, OnChanges } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-saved-groups',
  templateUrl: './saved-groups.component.html',
  styleUrls: ['./saved-groups.component.scss']
})
export class SavedGroupsComponent implements OnInit, OnChanges {

  @Input("saves") savesTab: Array<string>;

  private tab = [];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.get_saves().subscribe((res: any[]) => {
      this.tab = res;
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.tab = changes.savesTab.currentValue;
  }

}
