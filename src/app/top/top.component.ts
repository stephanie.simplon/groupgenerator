import { Component, OnInit, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { DataService } from '../data.service';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.scss']
})

export class TopComponent implements OnInit {

  private groups = [];
  private names = [];

  addNameValue: string;

  constructor(private http: HttpClient, private dataService: DataService) { }

  ngOnInit() {
  }

  @Output() newGroup = new EventEmitter<any>();
  @Output() newName = new EventEmitter<any>();

  onFormSubmit(userForm: NgForm) {
    if (userForm.value.firstname) {
      this.http.post('http://localhost:3000/new/user', {
        firstname: userForm.value.firstname,
      })
      .subscribe(
        res => {
          this.addNameValue = '';
          this.dataService.get_firstnames().subscribe((res: any[]) => {
            this.names = res;
            this.newName.emit(this.names);
          })
        },
        err => {
          console.log("Error occured");
        }
      );
    }
  }

  makeGroups() {
    let valeur = (document.querySelector('input[name=choix]:checked') as HTMLInputElement).value;
    this.dataService.get_polynomes(parseInt(valeur)).subscribe((res: any[]) => {
      this.groups = res;
      this.newGroup.emit(this.groups);
    });
  }

}