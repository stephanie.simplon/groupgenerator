import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  private newGroups = [];
  private newNames = [];
  private newSaves = [];

  constructor() { }

  onNames(event) {
    this.newNames = event;
  }

  onGroups(event) {
    this.newGroups = event;
  }

  onSaves(event) {
    this.newSaves = event;
    console.log(this.newSaves);
  }

}