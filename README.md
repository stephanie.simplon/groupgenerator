Installer MongoDB et le lancer sur le port 27018

Effectuer un "npm install" dans les répertoires ./API/ et à la racine du projet ./

Dans ./API : lancer node ou nodemon
Dans ./ : lancer ng serve

Dans le navigateur, pour accéder au front, utiliser l'url http://localhost:4200

L'URL de l'API est http://localhost:3000